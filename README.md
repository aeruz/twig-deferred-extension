# Aeruz Twig Deferred Extension


> This extension is a full rework of an original idea of Eugene Leonovich (rybakit) and his [Twig Deferred Extension](https://github.com/rybakit/twig-deferred-extension).

This rework is a complete rewrite of the original extension, with the goal of making it work correctly (notably on overriding of block) and support future Twig 4.x versions and support only Twig since version 3.x.

## Twig version support

Starting with twig 3.9 a new rendering strategy is used, which introduced some side effects in this extensions.
So we need to split support of twig 3.x branches as indicated below :

| Deferred Extension version                                           | Twig version        | PHP version |
|----------------------------------------------------------------------|---------------------|-------------|
| >= 1.0.0, <= 1.0.2                                                   | >= 3.0.0, < 3.9.0   | >= 7.4      |
| unsupported due to issue https://github.com/twigphp/Twig/issues/4008 | 3.9.0, 3.9.1, 3.9.2 |             |
| >= 1.1.0, <= 1.2.2                                                   | >= 3.9.2, < 3.12.0  | >= 7.4      |
| >= 1.3.0, <= 1.3.1                                                   | >= 3.12.0, < 3.15.0 | >= 8.0.2    |
| >= 1.3.2, < 2.0.0                                                    | >= 3.15.0, < 4.0.0  | >= 8.0.2    |

> The new rendering strategy is supported only starting with version 1.2.0

Future versions

| Deferred Extension version                                           | Twig version        | PHP version |
|----------------------------------------------------------------------|---------------------|-------------|
| >= 2.0.0                                                             | >= 4.0.0            | >= 8.0.2    |

---

An extension for [Twig](https://twig.symfony.com/) that allows to defer block rendering.


## Installation

```bash
composer require aeruz/twig-deferred-extension
```


## Initialization

```php
use Twig\DeferredExtension\DeferredExtension;
use Twig\Environment;

...

$twig = new Environment($loader);
$twig->addExtension(new DeferredExtension());
```

## Simple example

```jinja
{% block foo deferred %}
    {{ bar }}
{% endblock %}

{% set bar = 'bar' %}
```

The `foo` block will output "bar".


## Advanced example

Just for example purposes, first create a [global twig variable](https://twig.symfony.com/doc/3.x/advanced.html#globals):

```php
use Twig\Environment;

...

$twig = new Environment($loader);
$twig->addGlobal('assets', new \ArrayObject());
```

Then build the following set of templates:

```jinja
{# layout.html.twig #}
<!DOCTYPE html>
<html>
    <head>
        ...
    </head>
    <body>
        {% block content '' %}

        {{ assets.append('/js/layout-header.js') }}

        {% block javascripts deferred %}
            {% for asset in assets %}
                <script src="{{ asset }}"></script>
            {% endfor %}
        {% endblock %}

        {{ assets.append('/js/layout-footer.js') }}
    </body>
</html>


{# page.html.twig #}
{% extends "layout.html.twig" %}

{% block content %}
    {{ assets.append('/js/page-header.js') }}

    {% if foo is defined %}
        {{ include("subpage1.html.twig") }}
    {% else %}
        {{ include("subpage2.html.twig") }}
    {% endif %}

    {{ assets.append('/js/page-footer.js') }}
{% endblock %}


{# subpage1.html.twig #}
{{ assets.append('/js/subpage1.js') }}


{# subpage2.html.twig #}
{{ assets.append('/js/subpage2.js') }}
```

The resulting html will be the following:

```html
<!DOCTYPE html>
<html>
    <head>
        ...
    </head>
    <body>
        <script src="/js/layout-header.js"></script>
        <script src="/js/page-header.js"></script>
        <script src="/js/subpage2.js"></script>
        <script src="/js/page-footer.js"></script>
        <script src="/js/layout-footer.js"></script>
    </body>
</html>
```


## Block overriding

```jinja
{# index.twig #}
{% extends "base.twig" %}
{% block foo %}foo is not deferred anymore{% endblock %}
{% block bar deferred %}bar is deferred now{% endblock %}

{# base.twig #}
{% block foo deferred %}foo is deferred{% endblock %}
{% block bar %}bar is not deferred{% endblock %}
```


## License

The library is released under the MIT License. See the bundled [LICENSE](LICENSE) file for details.
