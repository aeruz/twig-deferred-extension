<?php declare(strict_types=1);
/*
 * Copyright (c) 2024 Gildas de Cadoudal for Aerouant Ruz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Aeruz\Twig\Deferred;

use Aeruz\Twig\Deferred\Node\BlockNode;
use Aeruz\Twig\Deferred\Node\Expression\ResolveExpression;
use Twig\Environment;
use Twig\Error\SyntaxError;
use Twig\Node\BodyNode;
use Twig\Node\EmptyNode;
use Twig\Node\Expression\Variable\LocalVariable;
use Twig\Node\ModuleNode;
use Twig\Node\Node;
use Twig\Node\Nodes;
use Twig\Node\PrintNode;
use Twig\Node\SetNode;
use Twig\NodeVisitor\NodeVisitorInterface;

final class NodeVisitor implements NodeVisitorInterface
{
    private bool $hasDeferred = false;

    public function enterNode(Node $node, Environment $env): Node
    {
        if (!$this->hasDeferred && $node instanceof BlockNode) {
            $this->hasDeferred = true;
        }

        return $node;
    }

    /**
     * @throws SyntaxError
     */
    public function leaveNode(Node $node, Environment $env): ?Node
    {
        if ($this->hasDeferred && $node instanceof ModuleNode) {
            if (!$node->hasNode('parent')) {
                $lineno = $node->getTemplateLine();

                $ref = new LocalVariable('deferred_capture', $lineno);
                $ref->setAttribute('always_defined', true);

                $resolveExpr = new ResolveExpression($ref);

                $captureNode = new SetNode(true, $ref, new Nodes([
                    $node->getNode('display_start'),
                    $node->getNode('body'),
                    $node->getNode('display_end'),
                ]), $lineno);
                $resolveNode = new PrintNode($resolveExpr, $lineno);

                $node->setNode('display_start', new EmptyNode());
                $node->setNode('body', new BodyNode([$captureNode]));
                $node->setNode('display_end', $resolveNode);
            }
            $this->hasDeferred = false;
        }

        return $node;
    }

    public function getPriority(): int
    {
        // we want to pass after all other to be sure to capture all display_start, body and display_end output
        return \PHP_INT_MAX;
    }
}
