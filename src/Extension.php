<?php declare(strict_types=1);
/*
 * Copyright (c) 2024 Gildas de Cadoudal for Aerouant Ruz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Aeruz\Twig\Deferred;

use Twig\Extension\AbstractExtension;
use Twig\Template;

final class Extension extends AbstractExtension
{
    private bool $resolving = false;
    private array $blocks = [];
    private array $context = [];

    public function getTokenParsers(): array
    {
        return [new TokenParser()];
    }

    public function getNodeVisitors(): array
    {
        return [new NodeVisitor()];
    }

    public function defer(Template $template, string $blockName, array $context, array $blocks): string
    {
        $id = uniqid(get_class($template) . '::' . $blockName . '_', true);
        $this->blocks[$id] = [$template, $blockName];
        return $id;
    }

    public function saveContext(array $context): void
    {
        $this->context = $context;
    }

    public function resolve(Template $template, string $output, array $context, array $blocks): string
    {
        if ($this->resolving) {
            return $output;
        }
        $this->resolving = true;
        $this->saveContext($context);

        do {
            $initialCount = count($this->blocks);
            foreach ($this->blocks as $key => [$blockTemplate, $blockName]) {
                if (strpos($output, $key) !== false) {
                    $blocks[$blockName] = [$blockTemplate, 'deferred_block_' . $blockName];
                    /** @psalm-suppress InternalMethod we doesn't see any alternative to this use of internal method */
                    $blockOutput = $template->renderBlock($blockName, $this->context, $blocks);

                    $output = str_replace($key, $blockOutput, $output);
                    unset($this->blocks[$key]);
                    $initialCount--;
                }
            }
        // while new deferred blocks was added, we loop agagin
        } while ($initialCount < count($this->blocks) && count($this->blocks) > 0);

        $this->resolving = false;
        return $output;
    }
}
