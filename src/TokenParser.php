<?php declare(strict_types=1);
/*
 * Copyright (c) 2024 Gildas de Cadoudal for Aerouant Ruz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Aeruz\Twig\Deferred;

use Aeruz\Twig\Deferred\Node\BlockNode;
use Aeruz\Twig\Deferred\Node\Expression\DeferExpression;
use Aeruz\Twig\Deferred\Node\Expression\SaveContextExpression;
use Twig\Error\SyntaxError;
use Twig\Node\BlockNode as TwigBlockNode;
use Twig\Node\BlockReferenceNode;
use Twig\Node\DoNode;
use Twig\Node\EmptyNode;
use Twig\Node\Expression\Filter\RawFilter;
use Twig\Node\Node;
use Twig\Node\Nodes;
use Twig\Node\PrintNode;
use Twig\Parser;
use Twig\Token;
use Twig\TokenParser\AbstractTokenParser;
use Twig\TokenParser\BlockTokenParser;
use Twig\TokenParser\TokenParserInterface;

/**
 * @psalm-suppress PropertyNotSetInConstructor for $this->parser
 */
final class TokenParser extends AbstractTokenParser
{
    private TokenParserInterface $blockTokenParser;

    public function __construct()
    {
        /** @psalm-suppress InternalClass, UnusedPsalmSuppress not really a pb because all methods used are defined in TokenParserInterface */
        $this->blockTokenParser = new BlockTokenParser();
    }

    public function setParser(Parser $parser): void
    {
        parent::setParser($parser);

        $this->blockTokenParser->setParser($parser);
    }

    public function parse(Token $token): Node
    {
        $stream = $this->parser->getStream();
        if ($stream->look(1)->test('deferred')) {
            $node = $this->parseDeferredBlock($token);
        } else {
            $node = $this->blockTokenParser->parse($token);
        }
        return $node;
    }

    public function getTag(): string
    {
        return 'block';
    }

    public function decideBlockEnd(Token $token): bool
    {
        return $token->test('endblock');
    }

    /**
     * @throws SyntaxError
     */
    private function parseDeferredBlock(Token $token): Node
    {
        $lineno = $token->getLine();
        $stream = $this->parser->getStream();
        $name = $stream->expect(Token::NAME_TYPE)->getValue();
        $stream->expect(Token::NAME_TYPE, 'deferred');

        $block = $this->createDeferredBlockNode(new TwigBlockNode($name, new EmptyNode(), $lineno));
        $this->parser->setBlock($name, $block);
        $this->parser->pushLocalScope();
        $this->parser->pushBlockStack($name);

        if ($stream->nextIf(Token::BLOCK_END_TYPE)) {
            $body = $this->parser->subparse([$this, 'decideBlockEnd'], true);
            if ($token = $stream->nextIf(Token::NAME_TYPE)) {
                $value = $token->getValue();

                if ($value != $name) {
                    /** @psalm-suppress InternalMethod */
                    throw new SyntaxError(\sprintf('Expected endblock for block "%s" (but "%s" given).', $name, $value), $stream->getCurrent()->getLine(), $stream->getSourceContext());
                }
            }
        } else {
            $body = new Nodes([
                new PrintNode($this->parser->getExpressionParser()->parseExpression(), $lineno),
            ]);
        }
        $stream->expect(Token::BLOCK_END_TYPE);

        $saveContextNode = new DoNode(new SaveContextExpression(), $lineno);
        $block->setNode('body', new Nodes([
            $body,
            $saveContextNode
        ]));
        $this->parser->popBlockStack();
        $this->parser->popLocalScope();

        return new BlockReferenceNode($name, $lineno);
    }

    private function createDeferredBlockNode(Node $block): BlockNode
    {
        $name = $block->getAttribute('name');
        $lineno = $block->getTemplateLine();

        $deferExpr = new DeferExpression($block);
        $rawFilter = new RawFilter($deferExpr, null, null, $lineno);
        $deferNode = new PrintNode($rawFilter, $lineno);

        $deferredBlock = new BlockNode($name, $deferNode, new EmptyNode(), $lineno);
        if ($sourceContext = $block->getSourceContext()) {
            $deferredBlock->setSourceContext($sourceContext);
        }

        return $deferredBlock;
    }
}
