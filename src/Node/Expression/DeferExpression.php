<?php declare(strict_types=1);
/*
 * Copyright (c) 2024 Gildas de Cadoudal for Aerouant Ruz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Aeruz\Twig\Deferred\Node\Expression;

use Aeruz\Twig\Deferred\Extension;
use Twig\Compiler;
use Twig\Node\Expression\AbstractExpression;
use Twig\Node\Node;

class DeferExpression extends AbstractExpression
{
    public function __construct(Node $node)
    {
        parent::__construct(['deferred_block' => $node]);
    }

    public function compile(Compiler $compiler): void
    {
        $class = Extension::class;
        if (!$compiler->getEnvironment()->hasExtension($class)) {
            // Compile a non-optimized call to trigger a \Twig\Error\RuntimeError, which cannot be a compile-time error
            $compiler->raw(sprintf('$this->env->getExtension(\'%s\')', $class));
        } else {
            $compiler->raw(sprintf('$this->extensions[\'%s\']', ltrim($class, '\\')));
        }
        $compiler->raw('->defer($this, ');
        $compiler->string($this->getNode('deferred_block')->getAttribute('name'));
        $compiler->raw(', $context, $blocks)');
    }
}