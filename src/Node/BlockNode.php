<?php declare(strict_types=1);
/*
 * Copyright (c) 2024 Gildas de Cadoudal for Aerouant Ruz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Aeruz\Twig\Deferred\Node;

use Twig\Attribute\YieldReady;
use Twig\Compiler;
use Twig\Node\BlockNode as TwigBlockNode;
use Twig\Node\Node;

#[YieldReady]
final class BlockNode extends TwigBlockNode
{
    public function __construct(string $name, Node $deferNode, Node $body, int $lineno)
    {
        parent::__construct($name, $body, $lineno);
        $this->setNode('defer', $deferNode);
    }

    public function compile(Compiler $compiler): void
    {
        $name = $this->getAttribute('name');
        $deferNode = $this->getNode('defer');
        $bodyNode = $this->getNode('body');

        $this->setNode('body', $deferNode);
        $this->compileFunction($compiler, 'block_'.$name);

        $this->setNode('body', $bodyNode);
        $this->compileFunction($compiler, 'deferred_block_'.$name);
    }

    private function compileFunction(Compiler $compiler, string $functionName): void
    {
        $compiler
            ->addDebugInfo($this)
            ->write(sprintf("public function %s(\$context, array \$blocks = [])\n", $functionName), "{\n")
            ->indent()
            ->write("\$macros = \$this->macros;\n")
        ;

        $compiler
            ->subcompile($this->getNode('body'))
            ->write("return; yield '';\n") // needed when body doesn't yield anything
            ->outdent()
            ->write("}\n\n")
        ;
    }
}
