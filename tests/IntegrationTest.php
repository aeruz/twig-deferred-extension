<?php declare(strict_types=1);
/*
 * Copyright (c) 2024 Gildas de Cadoudal for Aerouant Ruz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Aeruz\Twig\Deferred\Tests;

use Aeruz\Twig\Deferred\Extension;
use Twig\Environment;
use Twig\Error\RuntimeError;
use Twig\Extension\StringLoaderExtension;
use Twig\Loader\ArrayLoader;
use Twig\Test\IntegrationTestCase;
use Twig\TwigFunction;

final class IntegrationTest extends IntegrationTestCase
{

    public function getExtensions() : array
    {
        return [
            new Extension(),
            new TestExtension(),
            new StringLoaderExtension(),
        ];
    }

    public function getFixturesDir() : string
    {
        return __DIR__.'/Fixtures';
    }

    public function testDeferredBlocksAreClearedOnRenderingError() : void
    {
        $loader = new ArrayLoader([
            'base' => '
                {%- block foo deferred -%}*{%- endblock -%}
                {{- block("foo") -}}
                {{- block("foo") -}}
                {% block content "base content" -%}
            ',
            'page_that_triggers_error' => '
                {% extends "base" %}
                {% block content %}{{ error() }}{% endblock %}
            ',
            'fallback_page' => '
                {% extends "base" %}
                {% block content "fallback content" %}
            ',
        ]);

        $twig = new Environment($loader, [
            'cache' => false,
            'strict_variables' => true,
            'use_yield' => false,
        ]);

        $twig->addExtension(new Extension());
        $twig->addFunction(new TwigFunction('error', static function () {
            throw new \RuntimeException('Oops');
        }));

        try {
            $result = $twig->render('page_that_triggers_error');
        } catch (RuntimeError $e) {
            self::assertSame(
                'An exception has been thrown during the rendering of a template ("Oops").',
                $e->getRawMessage()
            );

            $result = $twig->render('fallback_page');
        }

        self::assertSame('***fallback content', $result);
    }

    public function testCaptureNode() : void
    {
        $loader = new ArrayLoader([
            'base2' => '
            {%- set tmp -%}
              {%- block foo "foo" -%}
              {%- block bar "bar" -%}
            {%- endset -%}
            {{- tmp -}}
            ',
        ]);

        $twig = new Environment($loader, [
            'cache' => false,
            'strict_variables' => true,
            'use_yield' => true,
        ]);

        $result = $twig->render('base2');
        self::assertSame('foobar', $result);
    }
}
