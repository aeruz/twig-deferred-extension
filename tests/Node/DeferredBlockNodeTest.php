<?php declare(strict_types=1);
/*
 * Copyright (c) 2024 Gildas de Cadoudal for Aerouant Ruz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Aeruz\Twig\Deferred\Tests\Node;

use Aeruz\Twig\Deferred\Node\BlockNode;
use Twig\Node\Expression\ConstantExpression;
use Twig\Node\Nodes;
use Twig\Node\PrintNode;
use Twig\Test\NodeTestCase;

class DeferredBlockNodeTest extends NodeTestCase
{
    public function getTests(): array
    {
        // $node, $source, $environment = null, $isPattern = false
        $cases = [];
        $cases['base'] = [
            new BlockNode('name',
                new Nodes([new PrintNode(new ConstantExpression('defer', 1), 1)]),
                new Nodes([new PrintNode(new ConstantExpression('body', 1), 1)]),
                1),
            <<<EOF
            // line 1
            public function block_name(\$context, array \$blocks = [])
            {
                \$macros = \$this->macros;
                yield "defer";
                return; yield '';
            }
            
            public function deferred_block_name(\$context, array \$blocks = [])
            {
                \$macros = \$this->macros;
                yield "body";
                return; yield '';
            }
            EOF
        ];

        return $cases;
    }
}
